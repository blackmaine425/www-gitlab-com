---
layout: handbook-page-toc
title: "GitLab Data Processing Agreement and Standard Contractual Clauses"
description: "This agreement ..."
---

<a href="https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/DPA_Signed_09_21_21.pdf">GitLab Data Processing Addendum</a>

<a href="https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/Exhibit_B__Standard_Contractual_Clauses_5_25_22.pdf"> Exhibit B - Standard Contractual Clauses</a>
